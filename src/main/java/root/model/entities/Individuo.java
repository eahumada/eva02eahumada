/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.model.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author eahumada
 */
@Entity
@Table(name = "individuo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Individuo.findAll", query = "SELECT p FROM Individuo p"),
    @NamedQuery(name = "Individuo.findById", query = "SELECT p FROM Individuo p WHERE p.id = :id"),
    @NamedQuery(name = "Individuo.findByNombres", query = "SELECT p FROM Individuo p WHERE p.nombres = :nombres"),
    @NamedQuery(name = "Individuo.findByApellido1", query = "SELECT p FROM Individuo p WHERE p.apellido1 = :apellido1"),
    @NamedQuery(name = "Individuo.findByApellido2", query = "SELECT p FROM Individuo p WHERE p.apellido2 = :apellido2"),
    @NamedQuery(name = "Individuo.findByCorreo", query = "SELECT p FROM Individuo p WHERE p.correo = :correo"),
    @NamedQuery(name = "Individuo.findByTelefono", query = "SELECT p FROM Individuo p WHERE p.telefono = :telefono")})
public class Individuo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    @Size(max = Integer.MAX_VALUE)
    @Column(name = "nombres")
    private String nombres;
    
    @Size(max = Integer.MAX_VALUE)
    @Column(name = "apellido1")
    private String apellido1;

    @Size(max = Integer.MAX_VALUE)
    @Column(name = "apellido2")
    private String apellido2;

    @Size(max = Integer.MAX_VALUE)
    @Column(name = "correo")
    private String correo;

    @Size(max = Integer.MAX_VALUE)
    @Column(name = "telefono")
    private String telefono;

    public Individuo() {
    }

    public Individuo(Integer id) {
        this.id = id;
    }

    public Individuo(String nombres, String apellido1, String apellido2, String correo, String telefono) {
        this.nombres = nombres;
        this.apellido1 = apellido1;
        this.apellido2 = apellido2;
        this.correo = correo;
        this.telefono = telefono;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombre(String nombre) {
        this.nombres = nombre;
    }

    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido1) {
        this.apellido2 = apellido2;
    }    
    
    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }
    
    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Individuo)) {
            return false;
        }
        Individuo other = (Individuo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "root.model.entities.Individuo { id: " + id + " }";
    }
    
}