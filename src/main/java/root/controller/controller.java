/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import root.model.dao.IndividuoDAO;
import root.model.entities.Individuo;

/**
 *
 * @author eahumada
 */
@WebServlet(name = "controller", urlPatterns = {"/controller"})
public class controller extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Integer id = null;
        String mensaje = "";
        String accion = request.getParameter("accion");

        Logger.getLogger("log").log(Level.INFO, "accion: {0}", String.valueOf(accion));
        
        Individuo individuo = new Individuo(
                (String) request.getParameter("nombres"),
                (String) request.getParameter("apellido1"),
                (String) request.getParameter("apellido2"),
                (String) request.getParameter("correo"),
                (String) request.getParameter("telefono")
        );

        if (request.getParameter("id") != null) {
            individuo.setId(Integer.parseInt(request.getParameter("id")));
        }

        IndividuoDAO dao = new IndividuoDAO();

        if (accion == null || "".equals(accion) || accion.equalsIgnoreCase("crear")) {
            // Creamos un nuevo individuo
            try {
                dao.create(individuo);
                Logger.getLogger("log").log(Level.INFO, "Registro de individuo ingresado: {0}", individuo.getId());
                mensaje = "El registro del individuo, ID:  " + individuo.getId() + " a sido agregado correctamante";

                // Probando el metodo merge
                individuo.setTelefono(individuo.getTelefono() + ".");
                dao.edit(individuo);

                // Probando el metodo destroy
                dao.destroy(individuo.getId());

                // Insertando nuevamente el individuo para que aseguranos que quede en la tabla
                dao.create(individuo);
                mensaje = String.format("Registro de individuo ingresado: %d", individuo.getId());
                Logger.getLogger("log").log(Level.INFO, mensaje);
                mensaje = "El registro del individuo, ID:  " + individuo.getId() + " a sido agregado correctamante";

            } catch (Exception ex) {
                mensaje = String.format("Error al ingresar el individuo %s", ex.getMessage());
                Logger.getLogger("log").log(Level.SEVERE, mensaje);
            }

        } else if (accion != null && "actualizar".equalsIgnoreCase(accion)) {
            // Modificamos un individuo existente
            try {
                Logger.getLogger("log").log(Level.INFO, "Registro de individuo modificado: {0}", individuo.getId());
                dao.edit(individuo);
                mensaje = "El registro del individuo, ID:  " + individuo.getId() + " a sido modificado correctamante";
            } catch (Exception ex) {
                Logger.getLogger(controller.class.getName()).log(Level.SEVERE, null, ex);
                Logger.getLogger("log").log(Level.SEVERE, "Error al modificar el individuo {0}", ex.getMessage());
            }
                        
        } else if (accion != null && "eliminar".equalsIgnoreCase(accion)) {
            // Eliminamos un individuo existente
            try {
                Logger.getLogger("log").log(Level.INFO, "Registro de individuo ingresado: {0}", individuo.getId());
                dao.destroy(individuo.getId());
                mensaje = "El registro del individuo, ID:  " + individuo.getId() + " a sido eliminado correctamante";
            } catch (Exception ex) {
                Logger.getLogger(controller.class.getName()).log(Level.SEVERE, null, ex);
                Logger.getLogger("log").log(Level.SEVERE, "Error al eliminar el individuo {0}", ex.getMessage());
            }
        }
        
        request.setAttribute("mensaje", mensaje);
        request.getRequestDispatcher("salida.jsp").forward(request, response);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
