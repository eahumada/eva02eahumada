<%-- 
    Document   : destroy
    Created on : 25-04-2020
    Author     : eahumada
--%>
<%@page import="root.model.entities.Individuo"%>
<%@page import="root.model.dao.IndividuoDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Evaluacion 02 - Persistencia.</title>
    </head>
    <%
        IndividuoDAO dao = new IndividuoDAO();
        int id = Integer.parseInt(request.getParameter("id"));
        Individuo individuo = dao.findIndividuo(id);
    %>

    <body>
    <center>
        <div>
            <br>
            <h2 class="text-center">¿DESEA ELIMINAR ESTE REGISTRO?</h2>
            <br>
        </div>
        <div>
            <br>
            <h4>ID : <%=individuo.getId()%></h4>
            <h4><%=individuo.getNombres()%> <%=individuo.getApellido1()%></h4>
            <br>
        </div>
        <div>
            <form action="controller" method="POST">
                <input type="hidden" name="id" value="<%=individuo.getId()%>" readonly/><br>
                <input type="submit" value="Eliminar" name="accion" />
                <a href="index.jsp">Volver</a>
            </form>
        </div>     
    </body>
</html>