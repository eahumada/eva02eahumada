<%-- 
    Document   : index
    Created on : Apr 19, 2020, 5:13:18 PM
    Author     : eahumada
--%>

<%@page import="java.util.List"%>
<%@page import="root.model.entities.Individuo"%>
<%@page import="root.model.dao.IndividuoDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Evaluacion 02 - Persistencia.</title>
    </head>
    <body>
        <h1>Formulario de Ingreso de Datos</h1>
        <p>
            <%
                String mensaje = (String) request.getAttribute("mensaje");
            %>
            <%= (mensaje!=null)?mensaje:"" %>
        </p>
        <div>
            <form name="form" action="controller" method="POST">
                Nombres: <br>
                <input type="text" name="nombres" value="" size="100" /><br>
                Apellido 1: <br>
                <input type="text" name="apellido1" value="" size="100" /><br>
                Apellido 2: <br>
                <input type="text" name="apellido2" value="" size="100" /><br>
                Correo: <br>
                <input type="text" name="correo" value="" size="100" /><br>
                Telefono: <br>
                <input type="text" name="telefono" value="" size="100" /><br>
                <input type="submit" value="Crear" name="accion" />
            </form>
        <div>
        <table>
            <thead>
                <tr>
                    <th>Id.</th>
                    <th>Nombre</th>
                    <th>Apellido1</th>
                    <th>Apellido2</th>
                    <th>Correo</th>
                    <th>Telefono</th>
                </tr>
            </thead>
            <tbody>
                <%
                    //codigo para llamar a la entidad, crear lista y desplegar
                    IndividuoDAO dao = new IndividuoDAO();
                    int items = dao.getCount();
                    List<Individuo> list = dao.findIndividuoEntities();
                    int indice = items - 1;
                    if (indice < 0) {
                        indice = 0;
                    }
                    if (items > 0) {
                        for (Individuo ls : list) {
                %>
                <tr>
                    <td><%=ls.getId()%></td>
                    <td><%=ls.getNombres()%></td>
                    <td><%=ls.getApellido1()%></td>
                    <td><%=ls.getApellido2()%></td>
                    <td><%=ls.getCorreo()%></td>
                    <td><%=ls.getTelefono()%></td>
                    <td>
                        <a href='edit.jsp?id=<%=ls.getId()%>'>Editar</a>
                        <a href='destroy.jsp?id=<%=ls.getId()%>'>Eliminar</a>
                    </td>                
                </tr>
                <% }
                } else {%>
            <h5 class='text-center'> NO HAY INDIVIDUOS INGRESADOS </h5>
            <%}%>
            </tbody>
        </table>
        
    </body>
</html>
