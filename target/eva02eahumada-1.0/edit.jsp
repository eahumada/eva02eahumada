<!DOCTYPE html>
<%-- 
    Document   : edit
    Created on : 25-04-2020
    Author     : eahumada
--%>
<%@page import="root.model.dao.IndividuoDAO"%>
<%@page import="java.util.List"%>
<%@page import="root.model.entities.Individuo"%>
<%@page import="root.controller.controller"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Evaluacion 02 - Persistencia.</title>
    </head>
    <%
        //codigo para llamar a la entidad, buscar persona segun id y desplegar en inputs correspondientes
        IndividuoDAO dao = new IndividuoDAO();
        int id = Integer.parseInt(request.getParameter("id"));
        Individuo individuo = dao.findIndividuo(id);
    %>

    <body>
    <center>
        <div>
            <br>
            <h1>MODIFICACION DE REGISTRO</h1>
            <p>Edicion de datos del individuo.</p>
            <br>
        </div>
        <div>
            <div >
                <form name="form" action="controller" method="POST">
                    Id: <br>
                    <input type="text" name="id" value="<%= individuo.getId() %>" size="100" readonly="readonly" /><br>
                    Nombres: <br>
                    <input type="text" name="nombres" value="<%= individuo.getNombres() %>" size="100" /><br>
                    Apellido 1: <br>
                    <input type="text" name="apellido1" value="<%= individuo.getApellido1() %>" size="100" /><br>
                    Apellido 2: <br>
                    <input type="text" name="apellido2" value="<%= individuo.getApellido2() %>" size="100" /><br>
                    Correo: <br>
                    <input type="text" name="correo" value="<%= individuo.getCorreo() %>" size="100" /><br>
                    Telefono: <br>
                    <input type="text" name="telefono" value="<%= individuo.getTelefono() %>" size="100" /><br>
                    <div>
                        <input type="submit" value="Actualizar" name="accion"/>
                        <a href="index.jsp" >Volver</a>
                    </div>
                </form>
            </div>
        </div>
        </div>
    </center>
</body>
</html>
